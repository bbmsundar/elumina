<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Gender;
use App\Department;
class Employee extends Model
{
    protected $table = 'employee';
    protected $guarded = [];

    public function getDeptIdAttribute() {
    	return Department::find($this->attributes['dept_id'])->name;
    }

    public function getGenderIdAttribute() {
    	return Gender::find($this->attributes['gender_id'])->name;
    }

}
