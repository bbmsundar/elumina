<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Gender;
use App\Employee;
use App\Department;
use Validator;

class EmployeeController extends Controller
{

	public function getEmployee(Request $request,$id=null) {
		$data['id'] = $id;
		$validation = Validator::make($data,[ 
				'id' => 'required|exists:employee',
				]);

		if($validation->fails()){
			$errors = $validation->errors();
			return $errors->toJson();	
		}

		$employee = Employee::find($id);
		return response()->json($employee,200);


	}

}
