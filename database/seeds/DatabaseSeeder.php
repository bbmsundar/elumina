<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
    	Model::unguard();

	    DB::table('gender')->insert([
	    	 'name' => 'female'
	    ]);
	    DB::table('gender')->insert([
	    	 'name' => 'male'
	    ]);

	   factory(App\Department::class, 10)->create();
	   factory(App\Employee::class, 1000)->create();

     //     factory(App\User::class, 50)->create()->each(function ($u) {
	    //     $u->posts()->save(factory(App\Post::class)->make());
	    // });
    }
}
